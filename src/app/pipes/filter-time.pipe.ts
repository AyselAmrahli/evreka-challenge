import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterTime'
})
export class FilterTimePipe implements PipeTransform {


  transform(items: any[], searchTime: string): any {
    if(!items) return [];
    if(!searchTime) return items;
    searchTime = searchTime.toLowerCase();
    return items.filter( item => {
      return item.time.toLowerCase().includes(searchTime);
    });
   }

}
