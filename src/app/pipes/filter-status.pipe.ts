import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterStatus'
})
export class FilterStatusPipe implements PipeTransform {

  transform(items: any[], searchStatus: string): any {
    if(!items) return [];
    if(!searchStatus) return items;
    searchStatus = searchStatus.toLowerCase();
    return items.filter( item => {
      return item.status.toLowerCase().includes(searchStatus);
    });
   }

}
