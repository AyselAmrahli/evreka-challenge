import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterDriver'
})
export class FilterDriverPipe implements PipeTransform {

  transform(items: any[], searchDriver: string): any {
    if(!items) return [];
    if(!searchDriver) return items;
    searchDriver = searchDriver.toLowerCase();
    return items.filter( item => {
      return item.driver.toLowerCase().includes(searchDriver);
    });
   }

}
