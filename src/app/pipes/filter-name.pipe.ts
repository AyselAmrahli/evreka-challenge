import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterName'
})
export class FilterNamePipe implements PipeTransform {

  transform(items: any[], searchVardiya: string): any {
    if(!items) return [];
    if(!searchVardiya) return items;
    searchVardiya = searchVardiya.toLowerCase();
    return items.filter( item => {
      return item.vardiya.toLowerCase().includes(searchVardiya);
    });
   }

}
