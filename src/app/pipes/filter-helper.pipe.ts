import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterHelper'
})
export class FilterHelperPipe implements PipeTransform {

  transform(items: any[], searchHelper: string): any {
    if(!items) return [];
    if(!searchHelper) return items;
    searchHelper = searchHelper.toLowerCase();
    return items.filter( item => {
      return item.helper.toLowerCase().includes(searchHelper);
    });
   }

}
