import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterVehicle'
})
export class FilterVehiclePipe implements PipeTransform {

  transform(items: any[], searchText: string): any {
    if(!items) return [];
    if(!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter( item => {
      return item.location.toLowerCase().includes(searchText);
    });
   }
  }


