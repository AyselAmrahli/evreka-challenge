import { Component, OnInit } from '@angular/core';
import { ReportsService } from 'src/app/services/reports.service';
import { ModalReportService } from 'src/app/services/modal-report.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  isModal: boolean = false;
  reports;
  

  modalReports: any;
  
  
  showModal(report) {
    this.modalReportService.modalReport = report;
    this.isModal = true;
  }
  

  hideModal() {

    this.isModal = false;
  }

  constructor(
    protected reportService: ReportsService,
    protected modalReportService: ModalReportService
  ) { }


 

  ngOnInit() {

    this.reports = this.reportService.getReports();
  }

}
