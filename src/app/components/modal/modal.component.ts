import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModalReportService } from 'src/app/services/modal-report.service';
import { ReportsService } from 'src/app/services/reports.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() modalReport: any;
  @Input() isModal: boolean;
  @Output() isModalChange: EventEmitter<number> =   new EventEmitter();

  reports: any;
  constructor(
    protected modalReportService: ModalReportService,
    protected reportService: ReportsService,

  ) { }


  hideModal() {
    this.isModalChange.emit();
  }

  ngOnInit() {

    this.modalReport = this.modalReportService.modalReport;
    this.reports = this.reportService.getReports();
  }

}
