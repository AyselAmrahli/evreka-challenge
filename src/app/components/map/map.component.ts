import { Component, OnInit } from '@angular/core';
import { ControlPosition } from '@agm/core/services/google-maps-types';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  title: string = 'Evreka AGM';
  lat: number = 21.7679;
  lng: number = 78.8718;


icon = "/assets/images/marker.svg";

onMapReady(map) {
  map.setOptions({
  zoomControl: 'true',
  zoomControlOptions: {
  position: ControlPosition.TOP_LEFT
  }
  });
  }

  markers = [
    {
      city: 'Lucknow',
      is_active: true,
      lng: 80.949997,
      lat: 26.850000
    },

    {
      city: 'Delhi',
      is_active: true,
      lng: 77.230003,
      lat: 28.610001
    },

    {
      city: 'Mumbai',
      is_active: true,
      lng: 72.877426,
      lat: 19.076090
    },

    {
      city: 'Gokak',
      is_active: true,
      lng: 74.833298,
      lat: 16.166700
    }
  ];

  constructor() { }



  ngOnInit() {
  }

}
