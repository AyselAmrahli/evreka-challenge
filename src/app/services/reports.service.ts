import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  reports = [
    {
      'vardiya': '13:30',
      'location': 'Fenertepe',
      'time': '13:12',
      'driver': 'Tanir Nalbant',
      'helper': '-',
      'performance':'66/103',
      'status': 'dispatched',
    },


    {
      'vardiya': '13:30',
      'location': 'Bogazkoy',
      'time': '13:11',
      'driver': 'Selcuk Yurt',
      'helper': '-',
      'performance':'58/85',
      'status': 'finished',
    },


    {
      'vardiya': '07:30',
      'location': 'Basaksehir',
      'time': '07:30',
      'driver': 'Emri Akca',
      'helper': '-',
      'performance':'108/148',
      'status': 'finihsed',
    },


    {
      'vardiya': '07:30',
      'location': '4.5.Etap',
      'time': '07:30',
      'driver': 'Yasar Demir',
      'helper': '-',
      'performance':'121/138',
      'status': 'finished',
    }
  ];

getReports() {
  return this.reports;
}

  constructor() { }
}
