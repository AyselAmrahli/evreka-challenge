import { TestBed } from '@angular/core/testing';

import { ModalReportService } from './modal-report.service';

describe('ModalReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModalReportService = TestBed.get(ModalReportService);
    expect(service).toBeTruthy();
  });
});
