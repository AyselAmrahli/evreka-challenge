import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { HeaderComponent } from './components/header/header.component';
import { ReportsComponent } from './components/reports/reports.component';
import { MapComponent } from './components/map/map.component';


import { AgmCoreModule } from '@agm/core';
import { MapFilterComponent } from './components/map/map-filter/map-filter.component';
import { FilterVehiclePipe } from './pipes/filter-vehicle.pipe';
import { FilterNamePipe } from './pipes/filter-name.pipe';
import { FilterTimePipe } from './pipes/filter-time.pipe';
import { FilterDriverPipe } from './pipes/filter-driver.pipe';
import { FilterHelperPipe } from './pipes/filter-helper.pipe';
import { FilterStatusPipe } from './pipes/filter-status.pipe';
import { ModalComponent } from './components/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ReportsComponent,
    MapComponent,
    MapFilterComponent,
    FilterVehiclePipe,
    FilterNamePipe,
    FilterTimePipe,
    FilterDriverPipe,
    FilterHelperPipe,
    FilterStatusPipe,
    ModalComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAAAgzs1wgbRaHBWyTiidXwlLrRip_vyEk'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
